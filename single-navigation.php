<?php get_header(); ?>
<?php the_post(); ?>

	<div class="post-txt-content">
		<div class="main-img">
			<?php the_post_thumbnail('full'); ?>
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="info-content">
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
		</div>
	</div>

<?php get_footer(); ?>