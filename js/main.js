jQuery(function($) {
	// One Page Scroll Settings
	if ( $('body').hasClass('home') ) {
		if ( $(window).width() < 768) { 
			$(".scroll").onepage_scroll({
			   sectionContainer: "section.mobile", 
			   animationTime: 500
			});
		} else {
			$(".scroll").onepage_scroll({
			   sectionContainer: "section.desktop", 
			   animationTime: 500
			});			
		}

		// Flexslider initialization 
	 	(function() {
		  	var $window = jQuery(window),
		      	flexslider;
		 
			function getGridSize() {
			    return (window.innerWidth < 568) ? 1 :
			    	   (window.innerWidth < 768) ? 2 : 3;
			}
			 
			$window.load(function() {
			    $('.flexslider').flexslider({
				      animation: "slide",
				      animationLoop: false,
				      itemWidth: 210,
				      itemMargin: 5,
				      minItems: getGridSize(), 
				      maxItems: getGridSize() 
			    });
			});
		 
		}());
	}

	// Icon Navigation
	$('.nav-block#nav1').addClass('active');
	$('.service').each(function() {
		$(this).on('click', function() {

			var section = $(this).data('section');
			var sectionIndex = $(this).data('index');

			if ( $(window).width() < 768) {

				$('.icon').removeClass('active');
				$(this).find('.icon').addClass('active');

				$('.onepage-pagination').removeClass('up').addClass('down');

				$('.onepage-pagination a.'+section+'').click();

			} else {

				if ( $(this).find('.icon').hasClass('active') ) {
					return false;
				}

				var sectionPos = ( $('.nav-block#'+section).position().left ) * 1;

				$('.icon, .nav-block').removeClass('active');
				$(this).find('.icon').addClass('active');
				$('.nav-block#'+section).addClass('active');



				$('.slidewrapper').animate({
					left: -sectionPos
				}, 200)



				$('.nav-block').not('.active').find('.content').animate({
					opacity: '0'
				}, 400)

				$('.nav-block.left-aligned').find('.content').animate({
					right: '-1000%'
				}, 500)
				$('.nav-block.right-aligned').find('.content').animate({
					left: '-1000%'
				}, 500)
				$('.nav-block.left-aligned#'+section).find('.image').animate({
					left: '-1000%'
				}, 500)

				$('.nav-block.right-aligned#'+section).find('.image').animate({
					right: '-1000%'
				}, 500)



				$('.nav-block.left-aligned#'+section).find('.image').delay(200).animate({
					left: '0'
				}, 500)

				$('.nav-block.right-aligned#'+section).find('.image').delay(200).animate({
					right: '-48%'
				}, 500)

				$('.nav-block.left-aligned#'+section).find('.content').delay(200).animate({
					opacity: '1',
					right: '0'
				}, 600)

				$('.nav-block.right-aligned#'+section).find('.content').delay(200).animate({
					opacity: '1',
					left: '0'
				}, 600)
			}
		})
	})



	// Board Tabs
	$('.tabs > div').on('click', function() {
		$('.tabs > div').removeClass('active');
		$(this).addClass('active');
	})
	$('.tabs > div').first().on('click', function() {
		$('.flight-arrival').hide();
		$('.flight').show();
	})
	$('.tabs > div').last().on('click', function() {
		$('.flight').hide();
		$('.flight-arrival').show();
	})


	// Mobile Menu
	$('.burger-btn').on('click', function() {
		$('.nav .menu').fadeToggle(600);
	})


	$('.info-content .footer, .page-content .footer').remove();


	// Navigation blocks animation
	if ( $(window).width() > 767 ) {
		$('.nav-block').width( $('.viewport').width() );

		$(window).resize(function() {
			$('.nav-block').width( $('.viewport').width() );			
		})
	}
})