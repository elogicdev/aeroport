<div id="post-sidebar">
 
    <?php if ( is_active_sidebar( 'weather' ) ) : ?>
 
        <?php dynamic_sidebar( 'weather' ); ?>
 
    <?php endif; ?>
 
</div>