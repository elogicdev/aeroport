<div class="top">
	<div class="avia-info">
		<p><span class="bold-txt"><?php echo tr($lang, 'avia_info'); ?>: </span><?php echo get_option('avia-info'); ?></p>
		<div class="social-media">
			<ul>
				<li><a href="<?php echo get_option('fb_link'); ?>"><i class="fa fa-facebook"></i></a></li>
				<li><a href="<?php echo get_option('vk_link'); ?>"><i class="fa fa-vk"></i></a></li>
				<li><a href="<?php echo get_option('tw_link'); ?>"><i class="fa fa-twitter"></i></a></li>
				<li><a href="<?php echo get_option('in_link'); ?>"><i class="fa fa-instagram"></i></a></li>
			</ul>
		</div>
	</div>
	<div class="lang-switcher">
		<p><a class="active" href="<?php echo $_SERVER['REQUEST_URI']; ?>">UKR</a> <a href="/en<?php echo $_SERVER['REQUEST_URI']; ?>">ENG</a></p>
	</div>
</div>
<div class="nav">
	<div class="logo">
		<a href="<?php echo get_home_url(); ?>">
			<?php if ( is_page() ) { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-violet.png" alt="" />
			<?php } else { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-white.png" alt="" />
			<?php } ?>
		</a>
	</div>
	<div class="menu">
		<?php wp_nav_menu('menu=main_menu&depth=1&menu_class=menu&container_class=nav-wrapped');?>				
	</div>
	<div class="weather">
		<div class="weather-sec">
            <div>
                <p class="celcius">
                	<?php get_sidebar(); ?>
                </p> 
            </div>
            <p>
            	<?php echo tr($lang, 'chernivtsi'); ?>, 

            	<?php   
            		$timedifference = +3;
				    $time_client = $timedifference * 3600;
					$real_time = time() + $time_client;
					echo strftime('%H:%M', $real_time);  
				?>
							
			</p>
            <p><?php echo tr($lang, 'date'); ?></p>
        </div>
	</div>
</div><!--nav-->