<section class="footer desktop mobile">
	<div class="footer-info">
		<div class="menu">
			<h2 class="footer-title"><?php echo tr($lang, 'menu'); ?></h2>
			<?php wp_nav_menu('menu=main_menu&depth=1&menu_class=menu&container_class=nav-wrapped');?>				
		</div>
		<div class="social-media">
			<h2 class="footer-title"><?php echo tr($lang, 'social_links'); ?></h2>
			<ul>
				<li><a href="<?php echo get_option('fb_link'); ?>"><i class="fa fa-facebook"></i></a></li>
				<li><a href="<?php echo get_option('vk_link'); ?>"><i class="fa fa-vk"></i></a></li>
				<li><a href="<?php echo get_option('tw_link'); ?>"><i class="fa fa-twitter"></i></a></li>
				<li><a href="<?php echo get_option('in_link'); ?>"><i class="fa fa-instagram"></i></a></li>
			</ul>
		</div>
		<div class="contacts">
			<h2 class="footer-title"><?php echo tr($lang, 'contacts'); ?></h2>
			<ul>
				<li><i class="fa fa-phone"></i><?php echo get_option('phone'); ?></li>
				<li><i class="fa fa-envelope"></i><a href="<?php echo get_option('mail'); ?>"><?php echo get_option('mail'); ?></a></li>
				<li><i class="fa fa-map-marker"></i><?php echo tr($lang, 'address'); ?></li>
			</ul>
		</div>
		<div class="logo">
			<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-white.png" alt="" />
			<p class="copyrights"><?php echo tr($lang, 'copyrights'); ?></p>
			<p class="copyrights"><?php echo tr($lang, 'design'); ?> <a target="_blank" href="https://elogic.co/">eLogic</a></p>
		</div>
	</div>
</section><!--footer-->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

<?php wp_footer(); ?>
</div>
</body>