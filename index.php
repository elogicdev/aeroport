<?php get_header(); ?>



<div class="page-container">
	<section id="services" class="desktop mobile">
		<h2 class="section-title"><?php echo tr($lang, 'navigation'); ?></h2>
		<div class="services-nav">
			<div class="service departing" data-section="nav1" data-index="3">
				<a class="icon active"></a>
				<h3><?php echo tr($lang, 'flight'); ?></h3>
			</div>
			<div class="service arriving" data-section="nav2" data-index="4">
				<a class="icon"></a>
				<h3><?php echo tr($lang, 'arriving'); ?></h3>
			</div>
			<div class="service waiting" data-section="nav3" data-index="5">
				<a class="icon"></a>
				<h3><?php echo tr($lang, 'waiting'); ?></h3>
			</div>
			<div class="service services" data-section="nav4" data-index="6">
				<a class="icon"></a>
				<h3><?php echo tr($lang, 'services'); ?></h3>
			</div>
			<div class="service documents" data-section="nav5" data-index="7">
				<a class="icon"></a>
				<h3><?php echo tr($lang, 'documents'); ?></h3>
			</div>

			<div class="flexslider mobile">
			  <ul class="slides">
			    <li class="service departing" data-section="nav1" data-index="3">
					<a class="icon"></a>
					<h3><?php echo tr($lang, 'flight'); ?></h3>
					<p><?php echo tr($lang, 'flight_txt'); ?></p>
			    </li>
			    <li class="service arriving" data-section="nav2" data-index="4">
					<a class="icon"></a>
					<h3><?php echo tr($lang, 'arriving'); ?></h3>
					<p><?php echo tr($lang, 'departing_txt'); ?></p>					
			    </li>
			    <li class="service waiting" data-section="nav3" data-index="5">
					<a class="icon"></a>
					<h3><?php echo tr($lang, 'waiting'); ?></h3>
					<p><?php echo tr($lang, 'waiting_txt'); ?></p>
			    </li>
			    <li class="service services" data-section="nav4" data-index="6">
					<a class="icon"></a>
					<h3><?php echo tr($lang, 'services'); ?></h3>
					<p><?php echo tr($lang, 'waiting_txt'); ?></p>
			    </li>
			    <li class="service documents" data-section="nav5" data-index="7">
					<a class="icon"></a>
					<h3><?php echo tr($lang, 'documents'); ?></h3>
					<p><?php echo tr($lang, 'documents_txt'); ?></p>
			    </li>			    
			    <!-- items mirrored twice, total of 12 -->
			  </ul>
			</div>
		</div><!--services-nav-->
		
		<div class="viewport">
			<ul class="slidewrapper">
				<?php navigation_section(); ?>
			</ul>
		</div>

	</section><!--services-->
	
	<div class="mobile-navigation">
		<?php navigation_section('mobile'); ?>
	</div> 

	<section id="partners" class="desktop mobile">
		<h2 class="section-title"><?php echo tr($lang, 'partners'); ?></h2>
		<div class="partners-section">
			<?php partners_section(); ?>
		</div><!--partners-section-->
	</section><!--partners-->

	<section id="about-city" class="desktop mobile" style="background: url(<?php echo get_option('about_city_img'); ?>) no-repeat center center;">
		<div class="button-wrapper">
			<a href="https://uk.wikipedia.org/wiki/%D0%A7%D0%B5%D1%80%D0%BD%D1%96%D0%B2%D1%86%D1%96">
				<span><?php echo tr($lang, 'learn_more'); ?></span>
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</section><!--about-city-->

</div><!--page-cantainer-->

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/onepage-scroll.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.onepage-scroll.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/flexslider-min.js"></script>

<?php get_footer(); ?>
