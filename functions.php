<?php

/// Scripts
wp_enqueue_script('jquery');

/// Post thumbnails
add_theme_support('post-thumbnails');


/// Widget shortcode
add_filter('widget_text', 'do_shortcode');


/// Menu
register_nav_menus(array(
	'main'  => 'Головне меню',    
	'page' 	=> 'Меню на сторінках'    
));


/// Header Logo
$args = array(
	'width'         => 195,
	'height'        => 30,
	'default-image' => get_template_directory_uri() . '/images/header-logo.jpg',
);
add_theme_support( 'custom-header', $args );



/// Excerpt length
function new_excerpt_length($length) {
	return 10;
}
add_filter('excerpt_length', 'new_excerpt_length');



function navigation_section($type = 'standard') {
	global $wp_query;

			
	$args = array(
	'posts_per_page' => 5,
	'post_type' 	 => 'navigation',
	'post_status'    => 'publish',
	'order'			 => 'DESC'
	);
	$posts = $wp_query->query( $args );
	$post_index = 0;

	foreach($posts as $post){ setup_postdata($post); 
		$post_index ++;

		if ($type == 'standard') { ?>
			<li class="nav-block <?php echo get_field('image_align', $post->ID);?>-aligned" id="nav<?php echo $post_index; ?>">
				<div class="content">
					<h3><?php echo $post->post_title; ?></h3>
					<p><?php the_excerpt(); ?></p>
					<div class="button-wrapper">
						<a href="<?php the_permalink($post->ID); ?>">
							<span><?php echo tr($lang, 'learn_more'); ?></span>
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>			
				<div class="image" style="background: url(<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
					<h2><?php echo $post->post_title; ?></h2>
				</div>
			</li>
		<?php 

		} elseif ($type == 'mobile') { ?>
			<section class="mobile nav-block <?php echo get_field('image_align', $post->ID);?>-aligned" id="nav<?php echo $post_index; ?>">
				<div class="content">
					<h3><?php echo $post->post_title; ?></h3>
					<p><?php the_excerpt(); ?></p>
					<div class="button-wrapper">
						<a href="<?php the_permalink($post->ID); ?>">
							<span><?php echo tr($lang, 'learn_more'); ?></span>
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>			
				<div class="image" style="background: url(<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
					<h2><?php echo $post->post_title; ?></h2>
				</div>
			</section>
		<?php }
		wp_reset_postdata();
	}
}




function partners_section() {
	global $wp_query;
			
	$args = array(
	'posts_per_page' => 3,
	'post_type' 	 => 'partners',
	'post_status'    => 'publish',
	'order'			 => 'ASK'
	);
	$posts = $wp_query->query( $args );

	foreach($posts as $post){ setup_postdata($post); 
		?>
			<a>
				<?php echo get_the_post_thumbnail($post->ID, 'medium'); ?>
			</a>
		<?php 
	} 
}




/// Сторінка налаштувань
// create custom plugin settings menu
add_action('admin_menu', 'baw_create_menu');

function baw_create_menu() {
	//create new top-level menu
	add_menu_page('Global Settings', 'Налаштування теми',  'administrator', __FILE__, 'baw_settings_page', 'dashicons-chart-bar');

	//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
}



function register_mysettings() {
	//register our settings
	register_setting( 'baw-settings-group', 'fb_link' );
	register_setting( 'baw-settings-group', 'vk_link' );
	register_setting( 'baw-settings-group', 'tw_link' );
	register_setting( 'baw-settings-group', 'in_link' );

	register_setting( 'baw-settings-group', 'tagline' );
	register_setting( 'baw-settings-group', 'tagline_en' );
	register_setting( 'baw-settings-group', 'address' );
	register_setting( 'baw-settings-group', 'address_en' );
	register_setting( 'baw-settings-group', 'mail' );
	register_setting( 'baw-settings-group', 'phone' );

	register_setting( 'baw-settings-group', 'avia-info' );

	register_setting( 'baw-settings-group', 'background-image' );
	register_setting( 'baw-settings-group', 'about_city_img' );
}

function baw_settings_page() {
?>
<div class="wrap">
	<h2>Налаштування</h2>

	<form method="post" action="options.php">
		<?php settings_fields( 'baw-settings-group' ); ?>
		
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
		
		<table class="form-table">

			<tr valign="top">
				<th scope="row">Слоган</th>
				<td><input type="text" name="tagline" style="width:65%" value="<?php echo get_option('tagline'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Слоган (en)</th>
				<td><input type="text" name="tagline_en" style="width:65%" value="<?php echo get_option('tagline_en'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Фон сайту</th>
				<td><input type="text" name="background-image" placeholder="http://..." style="width:65%" value="<?php echo get_option('background-image'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Картинка (Про місто)</th>
				<td><input type="text" name="about_city_img" placeholder="http://..." style="width:65%" value="<?php echo get_option('about_city_img'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Посилання Facebook</th>
				<td><input type="text" name="fb_link" style="width:65%" value="<?php echo get_option('fb_link'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Посилання Vkontakte</th>
				<td><input type="text" name="vk_link" style="width:65%" value="<?php echo get_option('vk_link'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Посилання Twitter</th>
				<td><input type="text" name="tw_link" style="width:65%" value="<?php echo get_option('tw_link'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Посилання Instagram</th>
				<td><input type="text" name="in_link" style="width:65%" value="<?php echo get_option('in_link'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Адреса</th>
				<td><input type="text" name="address" style="width:65%" value="<?php echo get_option('address'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Адреса (en)</th>
				<td><input type="text" name="address_en" style="width:65%" value="<?php echo get_option('address_en'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Мейл</th>
				<td><input type="text" name="mail" style="width:65%" value="<?php echo get_option('mail'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Телефон</th>
				<td><input type="text" name="phone" style="width:65%" value="<?php echo get_option('phone'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row">Авіадовідка</th>
				<td><input type="text" name="avia-info" style="width:65%" value="<?php echo get_option('avia-info'); ?>"/></td>
			</tr>			
		</table>
		
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>

	</form>
</div>

<?php
}			


function tr($lang = 'ua', $word = ''){
	global $tr;
	  $url = $_SERVER['REQUEST_URI'];
	
	if (substr_count( $url, '/en/')){
				$lang = 'en';
						}else{
				$lang = 'ua';
					}
				
	return $tr[$lang][$word];

}

$monthes = array(
    1 => 'Січня', 2 => 'Лютого', 3 => 'Березня', 4 => 'Квітня',
    5 => 'Травня', 6 => 'Червня', 7 => 'Липня', 8 => 'Серпня',
    9 => 'Вересня', 10 => 'Жовтня', 11 => 'Листопада', 12 => 'Грудня'
);

$days = array(
    'Нд', 'Пн', 'Вт', 'Ср',
    'Чт', 'Пн', 'Сб'
);


$tr['en']['tagline'] = htmlspecialchars_decode(get_option('tagline_en'));
$tr['en']['flight'] = 'Flight';
$tr['en']['departing'] = 'Departing';
$tr['en']['arriving'] = 'Arriving';
$tr['en']['waiting'] = 'Waiting';
$tr['en']['services'] = 'Services';
$tr['en']['documents'] = 'Documents';
$tr['en']['flight_txt'] = 'Baggage , transport and parking';
$tr['en']['departing_txt'] = ' Baggage , transport and parking';
$tr['en']['waiting_txt'] = 'Baggage , transport and parking';
$tr['en']['services_txt'] = 'Baggage , transport and parking';
$tr['en']['documents_txt'] = 'Transport control';
$tr['en']['learn_more'] = 'Learn more';
$tr['en']['menu'] = 'Menu';
$tr['en']['social_links'] = 'Social Media';
$tr['en']['contacts'] = 'Contacts';
$tr['en']['copyrights'] = '&copy; 2016 All Rights Reserved';
$tr['en']['design'] = 'Designed by';
$tr['en']['address'] = get_option('address_en');
$tr['en']['navigation'] = 'Navigation';
$tr['en']['partners'] = 'Our Partners';
$tr['en']['avia_info'] = 'Avia Info';
$tr['en']['chernivtsi'] = 'Chernivtsi';
$tr['en']['date'] = (date("D, M j Y"));



$tr['ua']['tagline'] = htmlspecialchars_decode(get_option('tagline'));
$tr['ua']['flight'] = 'Політ';
$tr['ua']['departing'] = 'Виліт';
$tr['ua']['arriving'] = 'Прибуття';
$tr['ua']['waiting'] = 'Очікування';
$tr['ua']['services'] = 'Послуги';
$tr['ua']['documents'] = 'Документи';
$tr['ua']['flight_txt'] = 'Видача багажу, транспорт і парковка';
$tr['ua']['departing_txt'] = 'Видача багажу, транспорт і парковка';
$tr['ua']['waiting_txt'] = 'Видача багажу, транспорт і парковка';
$tr['ua']['services_txt'] = 'Видача багажу, транспорт і парковка';
$tr['ua']['documents_txt'] = 'Паспортний контроль';
$tr['ua']['learn_more'] = 'Дізнатися більше';
$tr['ua']['menu'] = 'Меню';
$tr['ua']['social_links'] = 'Соціальні мережі';
$tr['ua']['contacts'] = 'Контакти';
$tr['ua']['copyrights'] = '&copy; 2016 Всі права захищені';
$tr['ua']['design'] = 'Розроблено в';
$tr['ua']['address'] = get_option('address');
$tr['ua']['navigation'] = 'Навігація';
$tr['ua']['partners'] = 'Наші Партнери';
$tr['ua']['avia_info'] = 'Авіадовідка';
$tr['ua']['chernivtsi'] = 'Чернівці';
$tr['ua']['date'] = ( $days[(date('w'))] . date(', d ') . $monthes[(date('n'))] . date(' Y'));




add_action( 'widgets_init', 'registriruem_sidebari' );
 
/// Register sidebars 
function registriruem_sidebari() {
register_sidebar(
  array(
    'id' => 'weather',
    'name' => __( 'Погода' ),
    'description' => __( 'Віджет для виводу погоди' ),
    'before_widget' => '<div id="%1$s" class="my_widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="my_widget-title">',
    'after_title' => '</h4>'
    )
   );
}