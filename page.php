<?php get_header(); ?>
<?php the_post(); ?>

<div class="page-content" style="background: url(<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
	<div class="info-page-content">
		<h2><?php the_title(); ?></h2>
		<div class="left">
			<?php the_post_thumbnail('large'); ?>
		</div>
		<div class="right">
			<?php  the_content(); ?>
		</div>		
	</div>
</div>


<?php get_footer(); ?>