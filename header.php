<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">		
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class($class); ?>>
<div class="scroll">
<section class="header desktop mobile">
<?php if (is_home()) { ?>
	<div class="main-banner">
		<div class="menu-mobile">	
			<div class="burger-btn">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</div>
			<div class="mobile-logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-white5.png" alt="" />
				</a>
			</div>
			<?php get_template_part( 'nav' ); ?>
		</div><!--menu-mobile-->

		<div class="banner-info">
			<div class="wellcome-txt">
			<p>
				<?php echo tr($lang, 'tagline'); ?> 
			</p>
			</div>
			<div class="board">
				<div>
					<div class="tabs">
						<div class="active"><?php echo tr($lang, 'departing'); ?></div>
						<div><?php echo tr($lang, 'arriving'); ?></div>
					</div>
					<div class="clearfix"></div>
					<div class="board-container">
						<div class="flight">
							<div>
								<ul>
									<li>15:00</li>
									<li>Київ</li>
									<li>PL 805</li>
									<li>Реєстрація</li>
								</ul>
							</div>
							<div>
								<ul>
									<li>15:00</li>
									<li>Київ</li>
									<li>PL 805</li>
									<li>Реєстрація</li>
								</ul>
							</div>
						</div>
						<div class="flight-arrival">
							<div>
								<ul>
									<li>16:00</li>
									<li>Мілан</li>
									<li>PL 805</li>
									<li>Реєстрація</li>
								</ul>
							</div>
							<div>
								<ul>
									<li>16:00</li>
									<li>Мілан</li>
									<li>PL 805</li>
									<li>Реєстрація</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--banner-info-->

		<div class="to-bottom">
			
		</div>

	</div>
	<?php } ?>
	
	<?php if ( is_singular() || is_archive() ) { ?>
		<div class="page-menu">
			<div class="menu-mobile">	
				<div class="burger-btn">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</div>
				<div class="mobile-logo">
					<a href="<?php echo get_home_url(); ?>">
						<?php if ( is_page() ) { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-stroke.png" alt="" />
						<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/img/aero-logo-white5.png" alt="" />
						<?php } ?>
					</a>
				</div>
				<?php get_template_part( 'nav' ); ?>
			</div><!--menu-mobile-->
		</div><!--page-menu-->
	<?php } ?>
</section><!--header-->


<style>
	html {
		margin-top: 0 !important;
	}
</style>
